const assert = require('assert');
const app = require('../../src/app');

describe('\'search\' service', () => {
  it('registered the service', () => {
    const service = app.service('search');

    assert.ok(service, 'Registered the service');
  });

  it('search result for web keyword', async () => {
    const result = await app.service('search').find({
      query: {
        keyword: 'web',
      },
    });

    assert.ok(result);
  });

  it('saves the keyword in history', async () => {
    const result = await app.service('search-history').find({
      query: {
        keyword: 'web',
      },
    });

    assert.ok(result);
  });
});
