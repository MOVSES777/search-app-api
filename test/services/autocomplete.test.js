const assert = require('assert');
const app = require('../../src/app');

describe('\'autocomplete\' service', () => {
  it('registered the service', () => {
    const service = app.service('autocomplete');

    assert.ok(service, 'Registered the service');
  });

  it('search result for web keyword', async () => {
    const result = await app.service('autocomplete').find({
      query: {
        keyword: 'web',
      },
    });

    assert.ok(result);
  });
});
