const assert = require('assert');
const app = require('../../src/app');

describe('\'search-history\' service', () => {
  it('registered the service', () => {
    const service = app.service('search-history');

    assert.ok(service, 'Registered the service');
  });
});
