module.exports = () => {
  return function(req, res, next) {
    req.params.ip = req.connection.remoteAddress;
    next();
  };
};
