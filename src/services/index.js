const search = require('./search/search.service.js');
const autocomplete = require('./autocomplete/autocomplete.service.js');
const searchHistory = require('./search-history/search-history.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(search);
  app.configure(autocomplete);
  app.configure(searchHistory);
};
