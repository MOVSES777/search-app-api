module.exports = {
  description: 'Autocomplete Services',
  definition: {
    type: 'object',
    required: ['keyword'],
    properties: {
      keyword: {
        type: 'string',
      },
    },
  },
};
