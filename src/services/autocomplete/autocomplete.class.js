const axios = require('axios').default;
const { BadRequest } = require('@feathersjs/errors');

/* eslint-disable no-unused-vars */
exports.Autocomplete = class Autocomplete {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    const apiUrl = this.app.get('AUTOCOMPLETE_API_URL');
    const { keyword } = params.query;
    if (keyword) {
      try {
        const response = await axios.get(`${apiUrl}${keyword}`);
        return response ? response.data : [];
      } catch (e) {
        new BadRequest(e);
      }
    }

    return [];
  }
};
