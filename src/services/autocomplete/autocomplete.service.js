// Initializes the `autocomplete` service on path `/autocomplete`
const { Autocomplete } = require('./autocomplete.class');
const hooks = require('./autocomplete.hooks');
const docs = require('./docs');

module.exports = function(app) {
  const options = {
    paginate: app.get('paginate'),
  };

  const autocomplete = new Autocomplete(options, app);
  autocomplete.docs = docs;
  // Initialize our service with any options it requires
  app.use('/autocomplete', autocomplete);

  // Get our initialized service so that we can register hooks
  const service = app.service('autocomplete');

  service.hooks(hooks);
};
