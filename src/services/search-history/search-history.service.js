// Initializes the `search-history` service on path `/search-history`
const { SearchHistory } = require('./search-history.class');
const createModel = require('../../models/search-history.model');
const hooks = require('./search-history.hooks');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/search-history', new SearchHistory(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('search-history');

  service.hooks(hooks);
};
