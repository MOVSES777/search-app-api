// Initializes the `search` service on path `/search`
const { Search } = require('./search.class');
const hooks = require('./search.hooks');
const addIpMiddleware = require('../../middleware/add-ip')();
const docs = require('./docs');

module.exports = function(app) {
  const options = {
    paginate: app.get('paginate'),
  };

  const search = new Search(options, app);
  search.docs = docs;
  // Initialize our service with any options it requires
  app.use('/search', search, addIpMiddleware);

  // Get our initialized service so that we can register hooks
  const service = app.service('search');

  service.hooks(hooks);
};
