module.exports = {
  description: 'Search Services',
  definition: {
    type: 'object',
    required: ['keyword'],
    properties: {
      keyword: {
        type: 'string',
      },
    },
  },
};
