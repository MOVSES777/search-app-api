const { BadRequest } = require('@feathersjs/errors');
const axios = require('axios');

exports.Search = class Search {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    const apiUrl = this.app.get('API_URL');
    const { keyword } = params.query;
    if (keyword) {
      this.app.service('search-history').create({
        keyword,
      });
      try {
        const response = await axios.get(`${apiUrl}?search=${keyword}`);
        return response ? response.data : [];
      } catch (e) {
        new BadRequest(e);
      }
    }

    return [];
  }
};
